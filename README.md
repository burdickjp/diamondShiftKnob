# Diamond faceted shift knob

## About
Developing and making this shift knob is documented [in this blog post](https://burdickjp.gitlab.io/2018/09/21/facetedShiftKnob.html).

## Design
The part was designed and CAM'd in [Autodesk Fusion 360](https://www.autodesk.com/products/fusion-360/overview). Design files are included in this repository. [Design files are also available on a360](https://a360.co/2OJlm2V).

## Stock
1.5 inch (38.1 mm) diameter 60 - 66 mm in length.  
Finished length is 60 mm.

## Tool table
| position | description                                | stick out |     tool holder |
|---------:|--------------------------------------------|----------:|-----------------|
|        1 | [[Haimer 3D-Taster NG]]                    |        27 | [tormach 33048] |
|        4 | [[Niagara N47734]] dia:8 3FL flat carb TiN |        32 |          [ER16] |
|        6 | [[Melin 19593]] dia:9.525 3FL ball carb    |        55 |          [ER32] |
|        7 | [[YG-1 0881KCN]] dia:8.8 2FL drill HSS Tin |        81 |          [ER32] |
|        9 | [[YG-1 D4107]] dia:8.4 2FL drill HSS TiN   |        37 |          [ER16] |

## Setups and operations
Either setup can be run first if match up between the two setups is good. Do not run operation 2 for the second setup. In general, it is best to run setup A first, as this puts the match up line between the sides and the taper.

### Setup A
Use a [4 inch vise](https://glacern.com/gpv_412). Use single vee block on traveling jaw.  

| G54 | description                |
|-----|----------------------------|
| X0  | center of stock +- 0.01 mm |
| Y0  | center of stock +- 0.01 mm |
| Z0  | top of model               |

Top of stock can be Z0 for finished surface or up to Z3. Top of vise should be lower than Z-32. 

#### Order of operations
| Operation         | Description         |
|-------------------|---------------------|
| top_01_3dadaptive | rough top, fillets  |
| top_02_2dcontour  | rough, finish sides |
| top_03_spiral     | finishing top       |
| top_04_3dcontour  | finish fillets      |

### Setup B
Use a [4 inch vise](https://glacern.com/gpv_412). Clamp on flats of sides.  
Indicate off of flats using long haimer probe.  

| G55 | description                              |
|-----|------------------------------------------|
| X0  | center of hexadecagonal prism +- 0.01 mm |
| Y0  | center of hexadecagonal prism +- 0.01 mm |
| Z0  | bottom of part                           |

Top of stock can be Z60 for finished surface up to Z63. Top of vise should be lower than Z32. 

#### Order of operations
| Operation                | Description          |
|--------------------------|----------------------|
| bottom_01_3dadaptive     | rough bottom, taper  |
| bottom_02_2dcontour      | rough, finish sides  |
| bottom_03_spiral         | finish bottom        |
| bottom_04_3dcontour      | finish taper         |
| bottom_05_drillstub      | start drill          |
| bottom_06_pocketcircular | rough, finish pocket |
| bottom_07_drillfull      | finish drill         |

## License
This repository is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.

[Tormach 33048]: https://www.tormach.com/store/index.php?app=ecom&ns=prodshow&ref=33048&portrelay=1
[ER32]: https://www.tormach.com/store/index.php?app=ecom&ns=prodshow&ref=33266&portrelay=1
[ER16]: https://www.tormach.com/store/index.php?app=ecom&ns=prodshow&ref=31831&portrelay=1
[Niagara N47734]: https://amzn.com/B003CP0WNU
[Melin 19593]: https://www.mcmaster.com/88825a56
[Haimer 3D-Taster NG]: https://www.haimer-usa.com/products/measuring-instruments/sensors/3d-sensor/3d-sensor-new-generation/3d-sensor-new-generation.html
[YG-1 0881KCN]: https://amzn.com/B009NOAYTG
[YG-1 D4107]: https://amzn.com/B009NNCIW8
