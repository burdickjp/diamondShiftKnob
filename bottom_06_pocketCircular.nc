(Mill - Circular Pocket G-code generated: Tue Sep 18 14:49:29 2018 )
(PathPilot Version: 2.1.3)
(Description = circularPocket)
(Material = Aluminum : 6061)

(Units = G21 mm)
(Work Offset = G54)
(Tool Number =  4)
(Tool Description = [Niagara N47734] dia:8 3FL flat carb TiN)
(Tool Diameter = 8.000 mm)
(Spindle RPM = 3470)

(Pocket Centers)
(    1  X = 0.000   Y = 0.000)

(Pocket Diameter = 11.000)
(Feed Rate = 540.5 mm/minute)
(Stepover = 1.824)

(Z Clear Location = 70.000)
(Z Start Location = 60.000 , End Location = 30.000)
(Z Depth of Cut = 2.000 , Adjusted = 2.000)
(Number of Z Passes = 15)
(Z Feed Rate = 173.0 mm/minute)


(----- Start of G-code -----)
(<cv1>)

G17 G90  (XY Plane, Absolute Distance Mode)
G64 P 0.130 Q 0.000 (Path Blending)
G21 (units in mm)
G55 (Work Offset)

G30 (Go to preset G30 location)
T 4 M6 G43 H 4

F 540.5 (Feed Rate, mm/minute)
S 3470 (RPM)
M8 (Coolant ON)
M3 (Spindle ON, Forward)

(*** Pocket 1 ***)

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 60.000
G1 Z 59.867
G0 Z 60.000

(Peck Cycle 1)
G0 Z 59.867
G1 Z 59.733
G0 Z 60.000

(Peck Cycle 2)
G0 Z 59.733
G1 Z 59.600
G0 Z 60.000

(Peck Cycle 3)
G0 Z 59.600
G1 Z 59.467
G0 Z 60.000

(Peck Cycle 4)
G0 Z 59.467
G1 Z 59.333
G0 Z 60.000

(Peck Cycle 5)
G0 Z 59.333
G1 Z 59.200
G0 Z 60.000

(Peck Cycle 6)
G0 Z 59.200
G1 Z 59.067
G0 Z 60.000

(Peck Cycle 7)
G0 Z 59.067
G1 Z 58.933
G0 Z 60.000

(Peck Cycle 8)
G0 Z 58.933
G1 Z 58.800
G0 Z 60.000

(Peck Cycle 9)
G0 Z 58.800
G1 Z 58.667
G0 Z 60.000

(Peck Cycle 10)
G0 Z 58.667
G1 Z 58.533
G0 Z 60.000

(Peck Cycle 11)
G0 Z 58.533
G1 Z 58.400
G0 Z 60.000

(Peck Cycle 12)
G0 Z 58.400
G1 Z 58.267
G0 Z 60.000

(Peck Cycle 13)
G0 Z 58.267
G1 Z 58.133
G0 Z 60.000

(Peck Cycle 14)
G0 Z 58.133
G1 Z 58.000
G0 Z 60.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 1)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 58.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 58.000
G1 Z 57.867
G0 Z 58.000

(Peck Cycle 1)
G0 Z 57.867
G1 Z 57.733
G0 Z 58.000

(Peck Cycle 2)
G0 Z 57.733
G1 Z 57.600
G0 Z 58.000

(Peck Cycle 3)
G0 Z 57.600
G1 Z 57.467
G0 Z 58.000

(Peck Cycle 4)
G0 Z 57.467
G1 Z 57.333
G0 Z 58.000

(Peck Cycle 5)
G0 Z 57.333
G1 Z 57.200
G0 Z 58.000

(Peck Cycle 6)
G0 Z 57.200
G1 Z 57.067
G0 Z 58.000

(Peck Cycle 7)
G0 Z 57.067
G1 Z 56.933
G0 Z 58.000

(Peck Cycle 8)
G0 Z 56.933
G1 Z 56.800
G0 Z 58.000

(Peck Cycle 9)
G0 Z 56.800
G1 Z 56.667
G0 Z 58.000

(Peck Cycle 10)
G0 Z 56.667
G1 Z 56.533
G0 Z 58.000

(Peck Cycle 11)
G0 Z 56.533
G1 Z 56.400
G0 Z 58.000

(Peck Cycle 12)
G0 Z 56.400
G1 Z 56.267
G0 Z 58.000

(Peck Cycle 13)
G0 Z 56.267
G1 Z 56.133
G0 Z 58.000

(Peck Cycle 14)
G0 Z 56.133
G1 Z 56.000
G0 Z 58.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 2)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 56.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 56.000
G1 Z 55.867
G0 Z 56.000

(Peck Cycle 1)
G0 Z 55.867
G1 Z 55.733
G0 Z 56.000

(Peck Cycle 2)
G0 Z 55.733
G1 Z 55.600
G0 Z 56.000

(Peck Cycle 3)
G0 Z 55.600
G1 Z 55.467
G0 Z 56.000

(Peck Cycle 4)
G0 Z 55.467
G1 Z 55.333
G0 Z 56.000

(Peck Cycle 5)
G0 Z 55.333
G1 Z 55.200
G0 Z 56.000

(Peck Cycle 6)
G0 Z 55.200
G1 Z 55.067
G0 Z 56.000

(Peck Cycle 7)
G0 Z 55.067
G1 Z 54.933
G0 Z 56.000

(Peck Cycle 8)
G0 Z 54.933
G1 Z 54.800
G0 Z 56.000

(Peck Cycle 9)
G0 Z 54.800
G1 Z 54.667
G0 Z 56.000

(Peck Cycle 10)
G0 Z 54.667
G1 Z 54.533
G0 Z 56.000

(Peck Cycle 11)
G0 Z 54.533
G1 Z 54.400
G0 Z 56.000

(Peck Cycle 12)
G0 Z 54.400
G1 Z 54.267
G0 Z 56.000

(Peck Cycle 13)
G0 Z 54.267
G1 Z 54.133
G0 Z 56.000

(Peck Cycle 14)
G0 Z 54.133
G1 Z 54.000
G0 Z 56.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 3)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 54.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 54.000
G1 Z 53.867
G0 Z 54.000

(Peck Cycle 1)
G0 Z 53.867
G1 Z 53.733
G0 Z 54.000

(Peck Cycle 2)
G0 Z 53.733
G1 Z 53.600
G0 Z 54.000

(Peck Cycle 3)
G0 Z 53.600
G1 Z 53.467
G0 Z 54.000

(Peck Cycle 4)
G0 Z 53.467
G1 Z 53.333
G0 Z 54.000

(Peck Cycle 5)
G0 Z 53.333
G1 Z 53.200
G0 Z 54.000

(Peck Cycle 6)
G0 Z 53.200
G1 Z 53.067
G0 Z 54.000

(Peck Cycle 7)
G0 Z 53.067
G1 Z 52.933
G0 Z 54.000

(Peck Cycle 8)
G0 Z 52.933
G1 Z 52.800
G0 Z 54.000

(Peck Cycle 9)
G0 Z 52.800
G1 Z 52.667
G0 Z 54.000

(Peck Cycle 10)
G0 Z 52.667
G1 Z 52.533
G0 Z 54.000

(Peck Cycle 11)
G0 Z 52.533
G1 Z 52.400
G0 Z 54.000

(Peck Cycle 12)
G0 Z 52.400
G1 Z 52.267
G0 Z 54.000

(Peck Cycle 13)
G0 Z 52.267
G1 Z 52.133
G0 Z 54.000

(Peck Cycle 14)
G0 Z 52.133
G1 Z 52.000
G0 Z 54.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 4)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 52.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 52.000
G1 Z 51.867
G0 Z 52.000

(Peck Cycle 1)
G0 Z 51.867
G1 Z 51.733
G0 Z 52.000

(Peck Cycle 2)
G0 Z 51.733
G1 Z 51.600
G0 Z 52.000

(Peck Cycle 3)
G0 Z 51.600
G1 Z 51.467
G0 Z 52.000

(Peck Cycle 4)
G0 Z 51.467
G1 Z 51.333
G0 Z 52.000

(Peck Cycle 5)
G0 Z 51.333
G1 Z 51.200
G0 Z 52.000

(Peck Cycle 6)
G0 Z 51.200
G1 Z 51.067
G0 Z 52.000

(Peck Cycle 7)
G0 Z 51.067
G1 Z 50.933
G0 Z 52.000

(Peck Cycle 8)
G0 Z 50.933
G1 Z 50.800
G0 Z 52.000

(Peck Cycle 9)
G0 Z 50.800
G1 Z 50.667
G0 Z 52.000

(Peck Cycle 10)
G0 Z 50.667
G1 Z 50.533
G0 Z 52.000

(Peck Cycle 11)
G0 Z 50.533
G1 Z 50.400
G0 Z 52.000

(Peck Cycle 12)
G0 Z 50.400
G1 Z 50.267
G0 Z 52.000

(Peck Cycle 13)
G0 Z 50.267
G1 Z 50.133
G0 Z 52.000

(Peck Cycle 14)
G0 Z 50.133
G1 Z 50.000
G0 Z 52.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 5)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 50.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 50.000
G1 Z 49.867
G0 Z 50.000

(Peck Cycle 1)
G0 Z 49.867
G1 Z 49.733
G0 Z 50.000

(Peck Cycle 2)
G0 Z 49.733
G1 Z 49.600
G0 Z 50.000

(Peck Cycle 3)
G0 Z 49.600
G1 Z 49.467
G0 Z 50.000

(Peck Cycle 4)
G0 Z 49.467
G1 Z 49.333
G0 Z 50.000

(Peck Cycle 5)
G0 Z 49.333
G1 Z 49.200
G0 Z 50.000

(Peck Cycle 6)
G0 Z 49.200
G1 Z 49.067
G0 Z 50.000

(Peck Cycle 7)
G0 Z 49.067
G1 Z 48.933
G0 Z 50.000

(Peck Cycle 8)
G0 Z 48.933
G1 Z 48.800
G0 Z 50.000

(Peck Cycle 9)
G0 Z 48.800
G1 Z 48.667
G0 Z 50.000

(Peck Cycle 10)
G0 Z 48.667
G1 Z 48.533
G0 Z 50.000

(Peck Cycle 11)
G0 Z 48.533
G1 Z 48.400
G0 Z 50.000

(Peck Cycle 12)
G0 Z 48.400
G1 Z 48.267
G0 Z 50.000

(Peck Cycle 13)
G0 Z 48.267
G1 Z 48.133
G0 Z 50.000

(Peck Cycle 14)
G0 Z 48.133
G1 Z 48.000
G0 Z 50.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 6)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 48.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 48.000
G1 Z 47.867
G0 Z 48.000

(Peck Cycle 1)
G0 Z 47.867
G1 Z 47.733
G0 Z 48.000

(Peck Cycle 2)
G0 Z 47.733
G1 Z 47.600
G0 Z 48.000

(Peck Cycle 3)
G0 Z 47.600
G1 Z 47.467
G0 Z 48.000

(Peck Cycle 4)
G0 Z 47.467
G1 Z 47.333
G0 Z 48.000

(Peck Cycle 5)
G0 Z 47.333
G1 Z 47.200
G0 Z 48.000

(Peck Cycle 6)
G0 Z 47.200
G1 Z 47.067
G0 Z 48.000

(Peck Cycle 7)
G0 Z 47.067
G1 Z 46.933
G0 Z 48.000

(Peck Cycle 8)
G0 Z 46.933
G1 Z 46.800
G0 Z 48.000

(Peck Cycle 9)
G0 Z 46.800
G1 Z 46.667
G0 Z 48.000

(Peck Cycle 10)
G0 Z 46.667
G1 Z 46.533
G0 Z 48.000

(Peck Cycle 11)
G0 Z 46.533
G1 Z 46.400
G0 Z 48.000

(Peck Cycle 12)
G0 Z 46.400
G1 Z 46.267
G0 Z 48.000

(Peck Cycle 13)
G0 Z 46.267
G1 Z 46.133
G0 Z 48.000

(Peck Cycle 14)
G0 Z 46.133
G1 Z 46.000
G0 Z 48.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 7)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 46.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 46.000
G1 Z 45.867
G0 Z 46.000

(Peck Cycle 1)
G0 Z 45.867
G1 Z 45.733
G0 Z 46.000

(Peck Cycle 2)
G0 Z 45.733
G1 Z 45.600
G0 Z 46.000

(Peck Cycle 3)
G0 Z 45.600
G1 Z 45.467
G0 Z 46.000

(Peck Cycle 4)
G0 Z 45.467
G1 Z 45.333
G0 Z 46.000

(Peck Cycle 5)
G0 Z 45.333
G1 Z 45.200
G0 Z 46.000

(Peck Cycle 6)
G0 Z 45.200
G1 Z 45.067
G0 Z 46.000

(Peck Cycle 7)
G0 Z 45.067
G1 Z 44.933
G0 Z 46.000

(Peck Cycle 8)
G0 Z 44.933
G1 Z 44.800
G0 Z 46.000

(Peck Cycle 9)
G0 Z 44.800
G1 Z 44.667
G0 Z 46.000

(Peck Cycle 10)
G0 Z 44.667
G1 Z 44.533
G0 Z 46.000

(Peck Cycle 11)
G0 Z 44.533
G1 Z 44.400
G0 Z 46.000

(Peck Cycle 12)
G0 Z 44.400
G1 Z 44.267
G0 Z 46.000

(Peck Cycle 13)
G0 Z 44.267
G1 Z 44.133
G0 Z 46.000

(Peck Cycle 14)
G0 Z 44.133
G1 Z 44.000
G0 Z 46.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 8)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 44.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 44.000
G1 Z 43.867
G0 Z 44.000

(Peck Cycle 1)
G0 Z 43.867
G1 Z 43.733
G0 Z 44.000

(Peck Cycle 2)
G0 Z 43.733
G1 Z 43.600
G0 Z 44.000

(Peck Cycle 3)
G0 Z 43.600
G1 Z 43.467
G0 Z 44.000

(Peck Cycle 4)
G0 Z 43.467
G1 Z 43.333
G0 Z 44.000

(Peck Cycle 5)
G0 Z 43.333
G1 Z 43.200
G0 Z 44.000

(Peck Cycle 6)
G0 Z 43.200
G1 Z 43.067
G0 Z 44.000

(Peck Cycle 7)
G0 Z 43.067
G1 Z 42.933
G0 Z 44.000

(Peck Cycle 8)
G0 Z 42.933
G1 Z 42.800
G0 Z 44.000

(Peck Cycle 9)
G0 Z 42.800
G1 Z 42.667
G0 Z 44.000

(Peck Cycle 10)
G0 Z 42.667
G1 Z 42.533
G0 Z 44.000

(Peck Cycle 11)
G0 Z 42.533
G1 Z 42.400
G0 Z 44.000

(Peck Cycle 12)
G0 Z 42.400
G1 Z 42.267
G0 Z 44.000

(Peck Cycle 13)
G0 Z 42.267
G1 Z 42.133
G0 Z 44.000

(Peck Cycle 14)
G0 Z 42.133
G1 Z 42.000
G0 Z 44.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 9)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 42.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 42.000
G1 Z 41.867
G0 Z 42.000

(Peck Cycle 1)
G0 Z 41.867
G1 Z 41.733
G0 Z 42.000

(Peck Cycle 2)
G0 Z 41.733
G1 Z 41.600
G0 Z 42.000

(Peck Cycle 3)
G0 Z 41.600
G1 Z 41.467
G0 Z 42.000

(Peck Cycle 4)
G0 Z 41.467
G1 Z 41.333
G0 Z 42.000

(Peck Cycle 5)
G0 Z 41.333
G1 Z 41.200
G0 Z 42.000

(Peck Cycle 6)
G0 Z 41.200
G1 Z 41.067
G0 Z 42.000

(Peck Cycle 7)
G0 Z 41.067
G1 Z 40.933
G0 Z 42.000

(Peck Cycle 8)
G0 Z 40.933
G1 Z 40.800
G0 Z 42.000

(Peck Cycle 9)
G0 Z 40.800
G1 Z 40.667
G0 Z 42.000

(Peck Cycle 10)
G0 Z 40.667
G1 Z 40.533
G0 Z 42.000

(Peck Cycle 11)
G0 Z 40.533
G1 Z 40.400
G0 Z 42.000

(Peck Cycle 12)
G0 Z 40.400
G1 Z 40.267
G0 Z 42.000

(Peck Cycle 13)
G0 Z 40.267
G1 Z 40.133
G0 Z 42.000

(Peck Cycle 14)
G0 Z 40.133
G1 Z 40.000
G0 Z 42.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 10)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 40.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 40.000
G1 Z 39.867
G0 Z 40.000

(Peck Cycle 1)
G0 Z 39.867
G1 Z 39.733
G0 Z 40.000

(Peck Cycle 2)
G0 Z 39.733
G1 Z 39.600
G0 Z 40.000

(Peck Cycle 3)
G0 Z 39.600
G1 Z 39.467
G0 Z 40.000

(Peck Cycle 4)
G0 Z 39.467
G1 Z 39.333
G0 Z 40.000

(Peck Cycle 5)
G0 Z 39.333
G1 Z 39.200
G0 Z 40.000

(Peck Cycle 6)
G0 Z 39.200
G1 Z 39.067
G0 Z 40.000

(Peck Cycle 7)
G0 Z 39.067
G1 Z 38.933
G0 Z 40.000

(Peck Cycle 8)
G0 Z 38.933
G1 Z 38.800
G0 Z 40.000

(Peck Cycle 9)
G0 Z 38.800
G1 Z 38.667
G0 Z 40.000

(Peck Cycle 10)
G0 Z 38.667
G1 Z 38.533
G0 Z 40.000

(Peck Cycle 11)
G0 Z 38.533
G1 Z 38.400
G0 Z 40.000

(Peck Cycle 12)
G0 Z 38.400
G1 Z 38.267
G0 Z 40.000

(Peck Cycle 13)
G0 Z 38.267
G1 Z 38.133
G0 Z 40.000

(Peck Cycle 14)
G0 Z 38.133
G1 Z 38.000
G0 Z 40.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 11)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 38.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 38.000
G1 Z 37.867
G0 Z 38.000

(Peck Cycle 1)
G0 Z 37.867
G1 Z 37.733
G0 Z 38.000

(Peck Cycle 2)
G0 Z 37.733
G1 Z 37.600
G0 Z 38.000

(Peck Cycle 3)
G0 Z 37.600
G1 Z 37.467
G0 Z 38.000

(Peck Cycle 4)
G0 Z 37.467
G1 Z 37.333
G0 Z 38.000

(Peck Cycle 5)
G0 Z 37.333
G1 Z 37.200
G0 Z 38.000

(Peck Cycle 6)
G0 Z 37.200
G1 Z 37.067
G0 Z 38.000

(Peck Cycle 7)
G0 Z 37.067
G1 Z 36.933
G0 Z 38.000

(Peck Cycle 8)
G0 Z 36.933
G1 Z 36.800
G0 Z 38.000

(Peck Cycle 9)
G0 Z 36.800
G1 Z 36.667
G0 Z 38.000

(Peck Cycle 10)
G0 Z 36.667
G1 Z 36.533
G0 Z 38.000

(Peck Cycle 11)
G0 Z 36.533
G1 Z 36.400
G0 Z 38.000

(Peck Cycle 12)
G0 Z 36.400
G1 Z 36.267
G0 Z 38.000

(Peck Cycle 13)
G0 Z 36.267
G1 Z 36.133
G0 Z 38.000

(Peck Cycle 14)
G0 Z 36.133
G1 Z 36.000
G0 Z 38.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 12)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 36.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 36.000
G1 Z 35.867
G0 Z 36.000

(Peck Cycle 1)
G0 Z 35.867
G1 Z 35.733
G0 Z 36.000

(Peck Cycle 2)
G0 Z 35.733
G1 Z 35.600
G0 Z 36.000

(Peck Cycle 3)
G0 Z 35.600
G1 Z 35.467
G0 Z 36.000

(Peck Cycle 4)
G0 Z 35.467
G1 Z 35.333
G0 Z 36.000

(Peck Cycle 5)
G0 Z 35.333
G1 Z 35.200
G0 Z 36.000

(Peck Cycle 6)
G0 Z 35.200
G1 Z 35.067
G0 Z 36.000

(Peck Cycle 7)
G0 Z 35.067
G1 Z 34.933
G0 Z 36.000

(Peck Cycle 8)
G0 Z 34.933
G1 Z 34.800
G0 Z 36.000

(Peck Cycle 9)
G0 Z 34.800
G1 Z 34.667
G0 Z 36.000

(Peck Cycle 10)
G0 Z 34.667
G1 Z 34.533
G0 Z 36.000

(Peck Cycle 11)
G0 Z 34.533
G1 Z 34.400
G0 Z 36.000

(Peck Cycle 12)
G0 Z 34.400
G1 Z 34.267
G0 Z 36.000

(Peck Cycle 13)
G0 Z 34.267
G1 Z 34.133
G0 Z 36.000

(Peck Cycle 14)
G0 Z 34.133
G1 Z 34.000
G0 Z 36.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 13)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 34.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 34.000
G1 Z 33.867
G0 Z 34.000

(Peck Cycle 1)
G0 Z 33.867
G1 Z 33.733
G0 Z 34.000

(Peck Cycle 2)
G0 Z 33.733
G1 Z 33.600
G0 Z 34.000

(Peck Cycle 3)
G0 Z 33.600
G1 Z 33.467
G0 Z 34.000

(Peck Cycle 4)
G0 Z 33.467
G1 Z 33.333
G0 Z 34.000

(Peck Cycle 5)
G0 Z 33.333
G1 Z 33.200
G0 Z 34.000

(Peck Cycle 6)
G0 Z 33.200
G1 Z 33.067
G0 Z 34.000

(Peck Cycle 7)
G0 Z 33.067
G1 Z 32.933
G0 Z 34.000

(Peck Cycle 8)
G0 Z 32.933
G1 Z 32.800
G0 Z 34.000

(Peck Cycle 9)
G0 Z 32.800
G1 Z 32.667
G0 Z 34.000

(Peck Cycle 10)
G0 Z 32.667
G1 Z 32.533
G0 Z 34.000

(Peck Cycle 11)
G0 Z 32.533
G1 Z 32.400
G0 Z 34.000

(Peck Cycle 12)
G0 Z 32.400
G1 Z 32.267
G0 Z 34.000

(Peck Cycle 13)
G0 Z 32.267
G1 Z 32.133
G0 Z 34.000

(Peck Cycle 14)
G0 Z 32.133
G1 Z 32.000
G0 Z 34.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 14)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 32.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

(Z Plunge Entry)

(Peck Length = 0.133)
G0 X 0.000 Y 0.000
G0 Z 70.000 (Z Clear)
F 173.0 (Z Feed, mm/minute)

(Peck Cycle 0)
G0 Z 32.000
G1 Z 31.867
G0 Z 32.000

(Peck Cycle 1)
G0 Z 31.867
G1 Z 31.733
G0 Z 32.000

(Peck Cycle 2)
G0 Z 31.733
G1 Z 31.600
G0 Z 32.000

(Peck Cycle 3)
G0 Z 31.600
G1 Z 31.467
G0 Z 32.000

(Peck Cycle 4)
G0 Z 31.467
G1 Z 31.333
G0 Z 32.000

(Peck Cycle 5)
G0 Z 31.333
G1 Z 31.200
G0 Z 32.000

(Peck Cycle 6)
G0 Z 31.200
G1 Z 31.067
G0 Z 32.000

(Peck Cycle 7)
G0 Z 31.067
G1 Z 30.933
G0 Z 32.000

(Peck Cycle 8)
G0 Z 30.933
G1 Z 30.800
G0 Z 32.000

(Peck Cycle 9)
G0 Z 30.800
G1 Z 30.667
G0 Z 32.000

(Peck Cycle 10)
G0 Z 30.667
G1 Z 30.533
G0 Z 32.000

(Peck Cycle 11)
G0 Z 30.533
G1 Z 30.400
G0 Z 32.000

(Peck Cycle 12)
G0 Z 30.400
G1 Z 30.267
G0 Z 32.000

(Peck Cycle 13)
G0 Z 30.267
G1 Z 30.133
G0 Z 32.000

(Peck Cycle 14)
G0 Z 30.133
G1 Z 30.000
G0 Z 32.000
G0 Z 70.000 (Z Clear)
F 540.5 (Feed, mm/minute)

(Spiral, Z Pass 15)
G0 X 0.000 Y 0.000
G0 Z 70.000
G0 Z 30.000

(Spiral)
F 2.5 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.000
F 3.4 (Arc Feed, mm/minute)
G1 X 0.025 Y 0.002
F 6.8 (Arc Feed, mm/minute)
G1 X 0.050 Y 0.009
F 10.1 (Arc Feed, mm/minute)
G1 X 0.073 Y 0.020
F 13.4 (Arc Feed, mm/minute)
G1 X 0.095 Y 0.035
F 16.6 (Arc Feed, mm/minute)
G1 X 0.115 Y 0.054
F 19.8 (Arc Feed, mm/minute)
G1 X 0.132 Y 0.076
F 22.9 (Arc Feed, mm/minute)
G1 X 0.145 Y 0.102
F 26.1 (Arc Feed, mm/minute)
G1 X 0.155 Y 0.130
F 29.1 (Arc Feed, mm/minute)
G1 X 0.161 Y 0.161
F 32.2 (Arc Feed, mm/minute)
G1 X 0.163 Y 0.194
F 35.2 (Arc Feed, mm/minute)
G1 X 0.160 Y 0.228
F 38.2 (Arc Feed, mm/minute)
G1 X 0.152 Y 0.263
F 41.1 (Arc Feed, mm/minute)
G1 X 0.139 Y 0.298
F 44.0 (Arc Feed, mm/minute)
G1 X 0.121 Y 0.333
F 46.9 (Arc Feed, mm/minute)
G1 X 0.098 Y 0.367
F 49.7 (Arc Feed, mm/minute)
G1 X 0.070 Y 0.399
F 52.5 (Arc Feed, mm/minute)
G1 X 0.038 Y 0.429
F 55.3 (Arc Feed, mm/minute)
G1 X 0.000 Y 0.456
F 58.1 (Arc Feed, mm/minute)
G1 X -0.042 Y 0.480
F 60.8 (Arc Feed, mm/minute)
G1 X -0.088 Y 0.499
F 63.4 (Arc Feed, mm/minute)
G1 X -0.138 Y 0.514
F 66.1 (Arc Feed, mm/minute)
G1 X -0.191 Y 0.524
F 68.7 (Arc Feed, mm/minute)
G1 X -0.246 Y 0.528
F 71.3 (Arc Feed, mm/minute)
G1 X -0.304 Y 0.527
F 73.9 (Arc Feed, mm/minute)
G1 X -0.363 Y 0.519
F 76.4 (Arc Feed, mm/minute)
G1 X -0.423 Y 0.505
F 78.9 (Arc Feed, mm/minute)
G1 X -0.484 Y 0.484
F 81.4 (Arc Feed, mm/minute)
G1 X -0.543 Y 0.456
F 83.9 (Arc Feed, mm/minute)
G1 X -0.602 Y 0.421
F 86.3 (Arc Feed, mm/minute)
G1 X -0.658 Y 0.380
F 88.7 (Arc Feed, mm/minute)
G1 X -0.712 Y 0.332
F 91.1 (Arc Feed, mm/minute)
G1 X -0.762 Y 0.277
F 93.4 (Arc Feed, mm/minute)
G1 X -0.808 Y 0.216
F 95.8 (Arc Feed, mm/minute)
G1 X -0.848 Y 0.150
F 98.1 (Arc Feed, mm/minute)
G1 X -0.883 Y 0.077
F 100.4 (Arc Feed, mm/minute)
G1 X -0.912 Y 0.000
F 102.6 (Arc Feed, mm/minute)
G1 X -0.934 Y -0.082
F 104.8 (Arc Feed, mm/minute)
G1 X -0.948 Y -0.167
F 107.1 (Arc Feed, mm/minute)
G1 X -0.954 Y -0.256
F 109.3 (Arc Feed, mm/minute)
G1 X -0.952 Y -0.347
F 111.4 (Arc Feed, mm/minute)
G1 X -0.941 Y -0.439
F 113.6 (Arc Feed, mm/minute)
G1 X -0.921 Y -0.532
F 115.7 (Arc Feed, mm/minute)
G1 X -0.892 Y -0.625
F 117.8 (Arc Feed, mm/minute)
G1 X -0.854 Y -0.716
F 119.9 (Arc Feed, mm/minute)
G1 X -0.806 Y -0.806
F 121.9 (Arc Feed, mm/minute)
G1 X -0.749 Y -0.893
F 124.0 (Arc Feed, mm/minute)
G1 X -0.683 Y -0.975
F 126.0 (Arc Feed, mm/minute)
G1 X -0.608 Y -1.053
F 128.0 (Arc Feed, mm/minute)
G1 X -0.525 Y -1.125
F 130.0 (Arc Feed, mm/minute)
G1 X -0.433 Y -1.190
F 132.0 (Arc Feed, mm/minute)
G1 X -0.334 Y -1.248
F 133.9 (Arc Feed, mm/minute)
G1 X -0.229 Y -1.297
F 135.8 (Arc Feed, mm/minute)
G1 X -0.117 Y -1.338
F 137.7 (Arc Feed, mm/minute)
G1 X -0.000 Y -1.368
F 139.6 (Arc Feed, mm/minute)
G1 X 0.121 Y -1.388
F 141.5 (Arc Feed, mm/minute)
G1 X 0.246 Y -1.397
F 143.4 (Arc Feed, mm/minute)
G1 X 0.374 Y -1.395
F 145.2 (Arc Feed, mm/minute)
G1 X 0.503 Y -1.381
F 147.0 (Arc Feed, mm/minute)
G1 X 0.632 Y -1.355
G1 X 0.750 Y -1.299

(Bottom)
G3 X 0.750 Y -1.299 I -0.750 J 1.299

G0 Z 70.000

F 540.5 (Feed Rate, mm/minute)
G0 Z 70.000 (Z Clear)

M9 (Coolant OFF)
M5 (Spindle OFF)

G30 Z 70.0 (Go in Z only to preset G30 location)

G30 (Go to preset G30 location)

M30 (End of Program)

