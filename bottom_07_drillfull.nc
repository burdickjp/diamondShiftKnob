(Mill - Pattern Drill G-code generated: Tue Sep 18 14:59:09 2018 )
(PathPilot Version: 2.1.3)
(Description = bottom_07_drillfull)
(Material = Aluminum : 6061)

(Units = G21 mm)
(Work Offset = G54)
(Tool Number =  7)
(Tool Description = [YG-1 0881KCN] dia:8.8 2FL drill HSS Tin)
(Tool Diameter = 8.800 mm)
(Spindle RPM = 2520)

(Z Clear = 70.000)
(Spot With Tool = N/A)
(Spot Depth = 2.032)
(Z Start Location = 30.000 , End Location = 5.000)
(Z Feed Rate = 292.4 mm/minute)
(Peck Depth = 4.000)
(Number of Pecks = 7)
(Adjusted Peck Depth = 3.571)
(Hole Bottom Dwell = 0.00)


(----- Start of G-code -----)
(<cv1>)

G17 G90  (XY Plane, Absolute Distance Mode)
G64 P 0.130 Q 0.000 (Path Blending)
G21 (units in mm)
G55 (Set Work Offset)

G30 (Go to preset G30 location)


(Drilling)
T 7 M6 G43 H 7

S 2520 (RPM)
M8 (Coolant ON)
M3 (Spindle ON, Forward)
F 292.4 (Z Feed, mm/minute)

G0 X 0.000 Y 0.000  (Hole 1 of 1)
G0 Z 70.000 (Z Clear)
G90 G98  (Absolute Distance Mode, Canned return to Z or R)
G83 Z 5.000 R 30.000 Q 3.571  (Canned Peck Drill)

G80 (Cancel canned cycle)

G0 Z 70.000

M9 (Coolant OFF)
M5 (Spindle OFF)

G30 Z 70.0 (Go in Z only to preset G30 location)

G30 (Go to preset G30 location)

M30 (End of Program)

