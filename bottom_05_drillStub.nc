(Mill - Pattern Drill G-code generated: Tue Sep 18 14:45:26 2018 )
(PathPilot Version: 2.1.3)
(Description = bottom_05_drillStub)
(Material = Aluminum : 6061)

(Units = G21 mm)
(Work Offset = G54)
(Tool Number =  9)
(Tool Description = [YG-1 D4107] dia:8.4 2FL drill HSS TiN)
(Tool Diameter = 8.400 mm)
(Spindle RPM = 2640)

(Z Clear = 70.000)
(Spot With Tool = N/A)
(Spot Depth = 0.000)
(Z Start Location = 60.000 , End Location = 30.000)
(Z Feed Rate = 301.6 mm/minute)
(Peck Depth = 4.000)
(Number of Pecks = 8)
(Adjusted Peck Depth = 3.750)
(Hole Bottom Dwell = 0.00)


(----- Start of G-code -----)
(<cv1>)

G17 G90  (XY Plane, Absolute Distance Mode)
G64 P 0.130 Q 0.000 (Path Blending)
G21 (units in mm)
G55 (Set Work Offset)

G30 (Go to preset G30 location)


(Drilling)
T 9 M6 G43 H 9

S 2640 (RPM)
M8 (Coolant ON)
M3 (Spindle ON, Forward)
F 301.6 (Z Feed, mm/minute)

G0 X 0.000 Y 0.000  (Hole 1 of 1)
G0 Z 70.000 (Z Clear)
G90 G98  (Absolute Distance Mode, Canned return to Z or R)
G83 Z 30.000 R 60.000 Q 3.750  (Canned Peck Drill)

G80 (Cancel canned cycle)

G0 Z 70.000

M9 (Coolant OFF)
M5 (Spindle OFF)

G30 Z 70.0 (Go in Z only to preset G30 location)

G30 (Go to preset G30 location)

M30 (End of Program)

